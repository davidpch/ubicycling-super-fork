# -*- mode: ruby -*-
# vi: set ft=ruby :

root = File.expand_path(File.dirname(__FILE__))

cookbook_path    [ "#{root}/../chef/cookbooks", "#{root}/../chef/site-cookbooks" ]
environment_path "#{root}/../chef/environments"
role_path        "#{root}/../chef/roles"
node_path        "#{root}/../chef/nodes"
data_bag_path    "#{root}/../chef/data_bags"

ssl_verify_mode :verify_peer
